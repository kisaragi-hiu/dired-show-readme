# dired-show-readme

Display README in a directory if it exists, like Github or Nextcloud.

![screenshot](https://kisaragi-hiu.com/static/dired-show-readme-0.2.1.png)

# 1.0 todos

- [X] Customize separator
- [x] Refresh content with revert-buffer
- [ ] Global minor mode
- [ ] Actually insert into buffer to fix scrolling and allow copying text or visiting links

## Install

With [`straight.el`](https://github.com/raxod502/straight.el):

```elisp
(straight-use-package
 '(dired-show-readme
   :host gitlab
   :repo "kisaragi-hiu/dired-show-readme"))

(add-hook 'dired-mode-hook #'dired-show-readme-mode)
```

Using `leaf`:

```elisp
(leaf dired-show-readme
  :straight (dired-show-readme :host gitlab :repo "kisaragi-hiu/dired-show-readme")
  :config
  (add-hook 'dired-mode-hook #'dired-show-readme-mode))
```

## Configuration

- `dired-show-readme-pandoc-executable`: Path to Pandoc, used to format the README
- `dired-show-readme-position`: `top` or `bottom`; where the README is displayed.

# License

GPL v3.
